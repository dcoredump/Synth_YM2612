#pragma once

#include <CircularBuffer.h>
#include <Audio.h>
#include <AudioStream.h>
#include "ym3438.h"

#define WRITE_BUFFER_SIZE 100

typedef struct {
  Bit8u reg;
  Bit16u data;
} write_buffer;

class AudioSynthYM3438 : public AudioStream
{
  public:
    const uint16_t audio_block_time_us = 1000000 / (AUDIO_SAMPLE_RATE / AUDIO_BLOCK_SAMPLES);
    uint32_t xrun = 0;
    uint16_t render_time_max = 0;

    AudioSynthYM3438() : AudioStream(0, NULL) {
      OPN2_Reset(&chip);
    };

    bool write(Bit8u part, Bit8u reg, Bit8u data)
    {
      if (ym3438_register.isFull())
        return (false);
      else
      {
        ym3438_register.push(write_buffer{reg, (Bit16u)(((part & 0x01) << 8) | data)});
        return (true);
      }
    }

    Bit8u read(Bit8u part)
    {
      Bit8u val = OPN2_Read(&chip, (Bit32u)0x4000 + (part<<1)); // Read in Z80 style

      return (val);
    }

    void update(void)
    {
      elapsedMicros render_time;
      audio_block_t *lblock;
      audio_block_t *rblock;

      if (in_update == true)
      {
        xrun++;
        in_update = false;
        return;
      }
      else
        in_update = true;

      lblock = allocate();
      rblock = allocate();

      if (lblock && rblock)
      {
        for (uint8_t i = 0; i < AUDIO_BLOCK_SAMPLES; i++)
        {
          Bit16s buffer[2] = { 0, 0 };

	  for (uint8_t op = 0; op < 24; op++)
          {
            Bit16s b[2] = { 0, 0 };
            
            OPN2_Clock(&chip, b);

	    buffer[0]+=b[0];
	    buffer[1]+=b[1];

            if ((data & (1<<15)) == 0)
            {
              if ((read(0) & 0x80) == 0)
              {
                OPN2_Write(&chip, 0x4001 + ((data & 0x0100)>>7), data & 0xff); // write data in Z80 style
                data = (1 << 15);
              }
            }
            else if (!ym3438_register.isEmpty())
            {
              if ((read(0) & 0x80) == 0)
              {
                write_buffer w = ym3438_register.shift();
                OPN2_Write(&chip, 0x4000 + ((w.data & 0x0100)>>7), w.reg); // Write reg in Z80 style
                data = w.data;
              }
            }
          }

      	  lblock->data[i] = buffer[0];
          rblock->data[i] = buffer[1];
        }
      }

      if (render_time > audio_block_time_us) // everything greater audio_block_time_us (2.9ms for buffer size of 128) is a buffer underrun!
        xrun++;

      if (render_time > render_time_max)
        render_time_max = render_time;

      if (lblock)
      {
        transmit(lblock, 0);
        release(lblock);
      }
      if (rblock)
      {
        transmit(rblock, 1);
        release(rblock);
      }

      in_update = false;
    };

  private:
    volatile bool in_update = false;
    ym3438_t chip;
    Bit16u data = (1 << 15);
    CircularBuffer<write_buffer, WRITE_BUFFER_SIZE> ym3438_register;
};
