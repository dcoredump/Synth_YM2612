#include <Audio.h>
#include "synth_ym3438.h"

AudioSynthYM3438         ym3438;
AudioPlayQueue           queue_l;
AudioPlayQueue           queue_r;
AudioOutputI2S           i2s1;
AudioOutputUSB           usb1;
AudioControlSGTL5000     sgtl5000;
AudioConnection          patchCord1(queue_l, 0, i2s1, 0);
AudioConnection          patchCord2(queue_r, 0, i2s1, 1);
AudioConnection          patchCord3(queue_l, 0, usb1, 0);
AudioConnection          patchCord4(queue_r, 0, usb1, 1);

#define NOTE_DELAY 1000

elapsedMillis timer;

void setup()
{
  delay(100);
  Serial.begin(230400);
  delay(50);

  Serial.println("<setup-start>");

  AudioMemory(16);

  sgtl5000.enable();
  sgtl5000.lineOutLevel(29);
  sgtl5000.dacVolumeRamp();
  sgtl5000.dacVolume(1.0);
  sgtl5000.unmuteHeadphone();
  sgtl5000.unmuteLineout();
  sgtl5000.volume(0.8, 0.8); // Headphone volume

  ym3438.write(0,0x22, 0); //LFO off
  ym3438.write(0,0x27, 0); //Channel 3 mode normal
  ym3438.write(0,0x28, 0); //All channels off
  ym3438.write(0,0x28, 1); //All channels off
  ym3438.write(0,0x28, 2); //All channels off
  ym3438.write(0,0x28, 3); //All channels off
  ym3438.write(0,0x28, 4); //All channels off
  ym3438.write(0,0x28, 5); //All channels off
  ym3438.write(0,0x2B, 0); //DAC off

  ym3438.write(0,0x30, 0x71); //DT1/MUL (multiplier) OP1
  ym3438.write(0,0x34, 0x0D); //DT1/MUL (multiplier) OP2
  ym3438.write(0,0x38, 0x33); //DT1/MUL (multiplier) OP3
  ym3438.write(0,0x3C, 0x01); //DT1/MUL (multiplier) OP4

  ym3438.write(0,0x40, 0x23); //Total Level OP1
  ym3438.write(0,0x44, 0x2D); //Total Level OP2
  ym3438.write(0,0x48, 0x26); //Total Level OP3
  ym3438.write(0,0x4C, 0x00); //Total Level OP4
  //envelopes
  ym3438.write(0,0x50, 0x5F); //RS/AR  OP1
  ym3438.write(0,0x54, 0x99); //RS/AR OP2
  ym3438.write(0,0x58, 0x5F); //RS/AR OP3
  ym3438.write(0,0x5C, 0x94); //RS/AR OP4

  ym3438.write(0,0x60, 5); //RS/AM  D1R
  ym3438.write(0,0x64, 5); //RS/AM D1R
  ym3438.write(0,0x68, 5); //RS/AM D1R
  ym3438.write(0,0x6C, 7); //RS/AM D1R

  ym3438.write(0,0x70, 2); //RS/ D2R
  ym3438.write(0,0x74, 2); //RS/D2R
  ym3438.write(0,0x78, 2); //RS/D2R
  ym3438.write(0,0x7C, 2); //RS/D2R

  ym3438.write(0,0x80, 0x11); //D1L
  ym3438.write(0,0x84, 0x11); //D1L
  ym3438.write(0,0x88, 0x11); //D1L
  ym3438.write(0,0x8C, 0xA6); //D1L

  ym3438.write(0,0x90, 0); //Proprietary/?????
  ym3438.write(0,0x94, 0); //Proprietary/?????
  ym3438.write(0,0x98, 0); //Proprietary/?????
  ym3438.write(0,0x9C, 0); //Proprietary/?????

  ym3438.write(0,0xb0, 0x32); //feedback/algo
  ym3438.write(0,0xb4, 0xC0); //both speakers on

  // MAGIC TIME!
  ym3438.write(0,0x28, 0); //key off

  ym3438.write(0,0xA4, 0x22); //set frequency (part 1 = octave)
  ym3438.write(0,0xA0, 0x69); //set frequency (part 2 = note)
  
  Serial.println("<setup-end>");
  Serial.flush();
}

void loop()
{
  Serial.println("Key-Down");
  ym3438.write(0,0x28, 0xC0); //key on
  delay(1000);
  Serial.println("Key-Up");
  ym3438.write(0,0x28, 0); //key off
  delay(1000);
}
